// [SECTION]While Loop

	/*
		- A while loop takes an expression/condition. 
		- Expression/s are any unit of code that can be evaluated.
			- if expression is true, the code block is executed.
		- Iteration - term given to the repitition of statements.

		Syntax:
			while(expression/condition){
				statement/code block
			}

	*/

let count =5;

while(count !== 0) {
	console.log("While: " + count);
		// count++; - this wil cause an infinite loop;
	count--;
};

// [SECTION] Do While Loop

		/*
				-A do-while loop works a lot like while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

				Syntax:
						do{
							statement
						} while(expression/condition)

		*/



// let number = Number(prompt("Give me a number."));

// do{
// 	console.log("Do while: " + number);
// 	// number++ (adds one after the condition has been met.)
// 	number += 1;
// } while(number < 10)

// [SECTION] For Loop
		/*
				- A foor loop is more flexible than while loop and do-while loop

				It consists of three parts:
						1. Initialization - value that will track the progression of the loop.
						2. Expression/Condition - the condition to be evaluated. This determines whether the loop will run one more time.
						3. Final Expression - indicates how to advance the loop (increment or decrement).


						Syntax:
								for(initialization; expression; final expression){
										statement;
										}
// 		*/
// // Increasing
// for(let count = 0; count <= 20; count++){
// 		console.log(count);
// };
// // Increment by 2s
// for(let count = 0; count <= 20; count+=2){
// 		console.log(count);
// };
// // Decreasing
// for(let count = 50; count >= 20; count--){
// 		console.log(count);
// };
//  // decrement by 2
// for(let count = 50; count >= 20; count--){
// 		console.log(count);
// };

// Looping with Strings
let myString = "alex";


// Characters in strings may be counter using the .length property
// Strings are special compared to other data types in a way that it has access to functions and other pieces of information compared to the primitive data types (char)
console.log	(myString.length);

// Accessing elements of a string 
// Individual characters of a string may be accessed using its index number.
console.log	(myString[0]); // this is "a"
console.log	(myString[1]); // this is "l"
console.log	(myString[2]); // this is "e"
console.log	(myString[3]); // this is "x"

// This loop will print out the individual letters of the myString variable
// condition -> 0 < 4
for(let x = 0; x < myString.length; x++){
	console.log	(myString[x]);
}

let myName = "AlExander"

for(let i=0;i<myName.length;i++){
	if(
			myName[i].toLowerCase()== "a" ||
			myName[i].toLowerCase()== "i" ||
			myName[i].toLowerCase()== "e" ||
			myName[i].toLowerCase()== "o" ||
			myName[i].toLowerCase()== "u" 

		) {
		// will print "Vowel" if we encountered a vowel letter from the string.
				console.log("Vowel")
	}else{
		// will print the non-vowel/consonant letters
		console.log(myName[i])
	}


}

// [SECTION] Continue and Break Statements

		//  The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all the statements in a code block. This is for efficiency of the program.

		// The "break" statement is used to terminate the current loop once a match has been found.

		for(let count = 0; count <= 20; count++){
			if(count % 2 === 0){
						// Tells the code to continue to the next iteration.
						// This ignores all the statements located after the continue statement.
						continue;
						console.log("Hi.")
			}
			// The numbers after 11 will no longer be printed.
			// console.log("Continue and Break: " + count);
			
			if(count > 10){
				break;
			}
			// The numbers after 10 will no longer be printed
			console.log("Continue and Break: " + count);
		}


		// Another example:
	  let name = "George";

		for(let i = 0; i < name.length; i++) {
			console.log(name[i]);
				if(name[i].toLowerCase() === "e"){
						console.log("Continue to the next iteration");
						continue;

				}		

					
					if(name[i].toLowerCase() === "r"){
						break;
					}







		}